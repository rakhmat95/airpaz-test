import React from 'react';

export default React.createContext({
  tasks: [],
  doneCounter: 0,
  addTask: (task: any) => {},
  deleteTask: (index: number) => {},
  moveTask: (currentIndex: number, newIndex: number) => {},
  incrementDoneCounter: () => {},
  decrementDoneCounter: () => {},
});
