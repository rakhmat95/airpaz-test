import React, {useState} from 'react';
import Context from './context';
import {switchItemArray} from '../utils/helpers';

const GlobalState = (props: any) => {
  const [tasks, setTasks] = useState<any>([]);
  const [doneCounter, setDoneCounter] = useState(0);

  const addTask = (task: any) => {
    const listTask = [...tasks, task];
    setTasks(listTask);
  };

  const deleteTask = (index: number) => {
    const listTask = [...tasks];
    listTask.splice(index, 1);
    setTasks(listTask);
  };

  const moveTask = (currentIndex: number, newIndex: number) => {
    const listTask = [...tasks];
    setTasks(switchItemArray(listTask, currentIndex, newIndex));
  };

  const incrementDoneCounter = () => {
    setDoneCounter(doneCounter + 1);
  };

  const decrementDoneCounter = () => {
    setDoneCounter(doneCounter - 1);
  };

  return (
    <Context.Provider
      value={{
        tasks: tasks,
        doneCounter: doneCounter,
        addTask: addTask,
        deleteTask: deleteTask,
        moveTask: moveTask,
        incrementDoneCounter: incrementDoneCounter,
        decrementDoneCounter: decrementDoneCounter,
      }}>
      {props.children}
    </Context.Provider>
  );
};

export default GlobalState;
