import {
  LayoutAnimation,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  UIManager,
  View,
} from 'react-native';
import React, {useContext, useState, useCallback} from 'react';
import Header from '../components/molecules/Header';
import context from '../context/context';
import ItemListCardTodo from '../components/organisms/ItemListCardTodo';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

type Props = {};

const MainScreen = (props: Props) => {
  const ctx = useContext(context);

  const [task, setTask] = useState('');

  const onChangeText = (text: string) => {
    setTask(text);
  };

  const onPressAdd = useCallback(() => {
    if (Platform.OS === 'android') {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    }
    ctx.addTask(task);
  }, [ctx, task]);

  const onPressUp = useCallback(
    (currentIndex: number) => {
      if (Platform.OS === 'android') {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      }
      ctx.moveTask(currentIndex, currentIndex - 1);
    },
    [ctx],
  );

  const onPressDown = useCallback(
    (currentIndex: number) => {
      if (Platform.OS === 'android') {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      }
      ctx.moveTask(currentIndex, currentIndex + 1);
    },
    [ctx],
  );

  const onPressDelete = useCallback(
    (currentIndex: number) => {
      if (Platform.OS === 'android') {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      }
      ctx.deleteTask(currentIndex);
    },
    [ctx],
  );

  return (
    <View style={styles.container}>
      <Header onChangeText={onChangeText} onPressAdd={onPressAdd} />
      <Text style={styles.textCounter}>Done: {ctx.doneCounter}</Text>
      <ScrollView>
        {ctx.tasks.map((item: any, index: number) => (
          <ItemListCardTodo
            key={index}
            index={index}
            taskName={item}
            onPressDown={onPressDown}
            onPressUp={onPressUp}
            onPressDelete={onPressDelete}
          />
        ))}
      </ScrollView>
    </View>
  );
};

export default MainScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: '#DDDEFA',
  },
  textCounter: {
    fontSize: 14,
    fontWeight: '500',
    color: '#000',
    marginBottom: 10,
  },
});
