import {Alert, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import InputField from '../atoms/InputField';
import ButtonAdd from '../atoms/ButtonAdd';

type Props = {
  onChangeText: (text: string) => void;
  onPressAdd: () => void;
};

const Header = (props: Props) => {
  const {onChangeText, onPressAdd} = props;

  return (
    <View style={styles.container}>
      <InputField placeholder="Task Name" onChangeText={onChangeText} />
      <ButtonAdd onPress={onPressAdd} />
    </View>
  );
};

export default React.memo(Header);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 10,
  },
});
