import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import ButtonUp from '../atoms/ButtonUp';
import ButtonDown from '../atoms/ButtonDown';

type Props = {
  disableUpBtn: boolean;
  disableDownBtn: boolean;
  onPressUp: () => void;
  onPressDown: () => void;
};

const ButtonSort = (props: Props) => {
  const {disableUpBtn, disableDownBtn, onPressUp, onPressDown} = props;

  return (
    <View style={styles.container}>
      <ButtonUp onPress={onPressUp} disabled={disableUpBtn} />
      <ButtonDown onPress={onPressDown} disabled={disableDownBtn} />
    </View>
  );
};

export default ButtonSort;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginRight: 10,
  },
});
