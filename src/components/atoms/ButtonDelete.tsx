import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

type Props = {
  onPress: () => void;
};

const ButtonDelete = (props: Props) => {
  return (
    <TouchableOpacity style={styles.button} onPress={props.onPress}>
      <Text style={styles.text}>Delete</Text>
    </TouchableOpacity>
  );
};

export default ButtonDelete;

const styles = StyleSheet.create({
  button: {
    padding: 10,
  },
  text: {
    fontSize: 12,
    fontWeight: '400',
    color: '#FA456A',
  },
});
