import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

type Props = {
  disabled?: boolean;
  onPress: () => void;
};

const ButtonDown = (props: Props) => {
  return (
    <TouchableOpacity
      disabled={props.disabled}
      style={[styles.button, props.disabled && {backgroundColor: '#FFFFFF60'}]}
      onPress={props.onPress}>
      <Text style={[styles.text, props.disabled && {color: '#00000060'}]}>
        Down
      </Text>
    </TouchableOpacity>
  );
};

export default ButtonDown;

const styles = StyleSheet.create({
  button: {
    padding: 6,
    backgroundColor: '#FFF',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 12,
    fontWeight: '400',
    color: '#000',
  },
});
