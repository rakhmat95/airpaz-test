import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

type Props = {
  disabled?: boolean;
  onPress: () => void;
};

const ButtonUp = (props: Props) => {
  return (
    <TouchableOpacity
      disabled={props.disabled}
      style={[styles.button, props.disabled && {backgroundColor: '#FFFFFF60'}]}
      onPress={props.onPress}>
      <Text style={[styles.text, props.disabled && {color: '#00000060'}]}>
        Up
      </Text>
    </TouchableOpacity>
  );
};

export default ButtonUp;

const styles = StyleSheet.create({
  button: {
    padding: 6,
    backgroundColor: '#FFF',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  text: {
    fontSize: 12,
    fontWeight: '400',
    color: '#000',
  },
});
