import {StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';

type Props = {
  placeholder: string;
  onChangeText: (text: string) => void;
};

const InputField = (props: Props) => {
  const {placeholder, onChangeText} = props;

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.textInput}
        placeholder={placeholder}
        onChangeText={onChangeText}
      />
    </View>
  );
};

export default InputField;

const styles = StyleSheet.create({
  container: {
    flex: 4,
    marginRight: 10,
  },
  textInput: {
    backgroundColor: '#FFF',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'gray',
    padding: 10,
  },
});
