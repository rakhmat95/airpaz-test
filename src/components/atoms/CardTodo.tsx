import {StyleSheet, Text, View} from 'react-native';
import React, {useState, useContext} from 'react';
import CheckBox from '@react-native-community/checkbox';
import context from '../../context/context';

type Props = {
  taskName: string;
};

const CardTodo = (props: Props) => {
  const ctx = useContext(context);

  const [isChecked, setChecked] = useState(false);

  return (
    <View style={styles.container}>
      <CheckBox
        value={isChecked}
        onValueChange={val => {
          if (val) {
            ctx.incrementDoneCounter();
          } else {
            ctx.decrementDoneCounter();
          }
          setChecked(val);
        }}
        style={styles.checkBox}
      />
      <Text
        style={[
          styles.text,
          isChecked && {textDecorationLine: 'line-through'},
        ]}>
        {props.taskName}
      </Text>
    </View>
  );
};

export default CardTodo;

const styles = StyleSheet.create({
  container: {
    flex: 4,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#FFF',
    height: '100%',
    borderRadius: 10,
  },
  checkBox: {
    marginRight: 5,
  },
  text: {
    fontSize: 14,
    color: '#000',
    fontWeight: '500',
  },
});
