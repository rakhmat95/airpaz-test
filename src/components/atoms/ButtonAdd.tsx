import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

type Props = {
  onPress: () => void;
};

const ButtonAdd = (props: Props) => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <Text style={styles.text}>Add</Text>
    </TouchableOpacity>
  );
};

export default ButtonAdd;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#3983D3',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#3983D3',
  },
});
