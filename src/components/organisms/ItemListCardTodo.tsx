import {Alert, StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';
import ButtonSort from '../molecules/ButtonSort';
import CardTodo from '../atoms/CardTodo';
import context from '../../context/context';
import ButtonDelete from '../atoms/ButtonDelete';

type Props = {
  index: number;
  taskName: string;
  onPressUp: (currentIndex: number) => void;
  onPressDown: (currentIndex: number) => void;
  onPressDelete: (currentIndex: number) => void;
};

const ItemListCardTodo = (props: Props) => {
  const ctx = useContext(context);

  const {index, taskName, onPressUp, onPressDown, onPressDelete} = props;

  return (
    <View style={styles.container}>
      <ButtonSort
        disableDownBtn={index === ctx.tasks.length - 1}
        disableUpBtn={index === 0}
        onPressDown={() => onPressDown(index)}
        onPressUp={() => onPressUp(index)}
      />
      <CardTodo taskName={taskName} />
      <ButtonDelete onPress={() => onPressDelete(index)} />
    </View>
  );
};

export default React.memo(ItemListCardTodo);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
});
