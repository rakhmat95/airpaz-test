import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import GlobalState from './context/GlobalState';
import MainScreen from './containers/MainScreen';

type Props = {};

const App = (props: Props) => {
  return (
    <GlobalState>
      <MainScreen />
    </GlobalState>
  );
};

export default App;

const styles = StyleSheet.create({});
